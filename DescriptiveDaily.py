import matplotlib.pyplot as plt
from DataAnalytics import read_data_file

def plot_daily_graph(left, tick_label, height1, height2):

    #left1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #height1 = [1,0,2,0,3,0,4,0,5,0,6]
    #tick_label1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

    #left2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #height2 = [0,-10,0, -24,0, -36,0, -40,0, -5,0]
    #tick_label2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

    # plotting a bar chart
    plt.bar(left, height1, tick_label = tick_label, width = 1, color = 'green')
    plt.bar(left, height2, tick_label = tick_label, width = 1, color = 'red')
    # naming the x-axis
    plt.xlabel('Day')
    # naming the y-axis
    plt.ylabel('Percent Change')
    # plot title
    plt.title('Daily Percentage Change Variation Over the Year')

    # function to show the plot
    plt.show()

def create_data_for_daily_plot(dataset):
    #try:
        left = []
        tick_label = []

        height1 = []
        height2 = []

        x = 0
        for key in dataset:
            x = x + 1

            left.append(x)
            #tick_label.append(dataset[key].get('MONTH') + "/" + dataset[key].get('DAY'))
            tick_label.append(dataset[key].get('MONTH') + dataset[key].get('DAY'))

            if (dataset[key].get('PCT_CHANGE') >= 0.0):
                height1.append(dataset[key].get('PCT_CHANGE'))
                height2.append(0)
            else:
                height1.append(0)
                height2.append(dataset[key].get('PCT_CHANGE'))
    #except ValueError:
    #    print("error")

        return left, tick_label, height1, height2

def plot_daily(fileName):
    dict_dataSet = read_data_file(fileName)
    left, tick_label, height1, height2 = create_data_for_daily_plot(dict_dataSet)
    plot_daily_graph(left, tick_label, height1, height2)

plot_daily('data_files\\1150_HISTORY_ADJUSTED_2019.xlsx')