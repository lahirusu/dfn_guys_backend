import matplotlib.pyplot as plt
from DataAnalytics import read_data_file

def plot_weekly_graph(left, tick_label, height1, height2):

    #left1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #height1 = [1,0,2,0,3,0,4,0,5,0,6]
    #tick_label1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

    #left2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #height2 = [0,-10,0, -24,0, -36,0, -40,0, -5,0]
    #tick_label2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

    # plotting a bar chart
    plt.bar(left, height1, tick_label = tick_label, width = 0.8, color = 'green')
    plt.bar(left, height2, tick_label = tick_label, width = 0.8, color = 'red')
    # naming the x-axis
    plt.xlabel('Week')
    # naming the y-axis
    plt.ylabel('Percent Change')
    # plot title
    plt.title('Weekly Percentage Change Variation Over the Year')

    # function to show the plot
    plt.show()

def create_data_for_weekly_plot(dataset):

    left = []
    tick_label = []

    height1 = []
    height2 = []

    x = 0
    prvWeek = 1
    pctChange = 0
    for key in dataset:
        x = x + 1
        week = dataset[key].get('WEEK')
        if (week == prvWeek):
            pctChange = pctChange + dataset[key].get('PCT_CHANGE')
        else:
            left.append(prvWeek)
            tick_label.append(prvWeek)

            if (pctChange >= 0.0):
                height1.append(pctChange)
                height2.append(0)
            else:
                height1.append(0)
                height2.append(pctChange)

            print("week: ", prvWeek, ", pctChange: ", pctChange)
            prvWeek = week
            pctChange = dataset[key].get('PCT_CHANGE')

    left.append(prvWeek)
    tick_label.append(prvWeek)
    if (pctChange >= 0.0):
        height1.append(pctChange)
        height2.append(0)
    else:
        height1.append(0)
        height2.append(pctChange)

    print("week: ", prvWeek, ", pctChange: ", pctChange)

    return left, tick_label, height1, height2

def plot_weekly(fileName):
    dict_dataSet = read_data_file(fileName)
    left, tick_label, height1, height2 = create_data_for_weekly_plot(dict_dataSet)
    plot_weekly_graph(left, tick_label, height1, height2)

plot_weekly('data_files\\1150_HISTORY_ADJUSTED_2019.xlsx')